# docker-hugo

Docker image of Alpine with Hugo, rsync, SSH and cURL.

All the tools needed to build and deploy an Hugo site!