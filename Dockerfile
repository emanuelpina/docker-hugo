FROM alpine:latest

ARG HUGO_VERSION

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            rsync \
            openssh-client \
            ca-certificates \
            git \
            curl \
            gcompat \
            libstdc++ \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*

WORKDIR /hugo

ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz /hugo/

RUN tar xzf hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz \
 && mv hugo /usr/local/bin/ \
 && rm hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz